import { shallowMount, createLocalVue} from '@vue/test-utils'
import VueRouter from 'vue-router'
import Shane from '@/views/ShanePage.vue'

const localVue = createLocalVue()
localVue.use(VueRouter)
//const router = new VueRouter()

const $route = {
  path: '/shane/'
}


describe("routes testing",  ()=>{
  const wrapper = shallowMount(Shane,{
    mocks: {
      $route
    },
  })
  const shanePageData = wrapper.vm.$data.greeting
  const expectedData = "Shane Page"
  it("Shane Route", ()=>{
    expect(shanePageData).toBe(expectedData)
  })
})

